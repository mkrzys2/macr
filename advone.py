#!/usr/bin/env python

       #$id = 0;
       #$email = $_POST['email'];
       #$username = $_POST['username'];
       #$password = $_POST['password'];
       #$major = $_POST['major'];
       #$interests = $_POST['interests'];
       #$age = $_POST['age'];
       #$grade = $_POST['grade'];
       #$gender = $_POST['gender'];

import sys

def main():
    
	merch_vocab = ["basketball", "baseball", "hockey", "soccer", "illinois", "football", "volleyball", "wrestling", "softball", "swimming", "sports", "excercise"]

	price_threshold = 10.00  
	age_threshold = 24  
    
    # merch, supplies, male, female, computery, category_of_item
	cat_to_promote = [1,1,1,1,1,1]
	
	hint = 0
	
	cat_index = int(sys.argv[17])
	cat_to_promote[cat_index] = cat_to_promote[cat_index] + 1
 
	if (sys.argv[6] in merch_vocab):
    		hint = 1
   		cat_to_promote[0] = cat_to_promote[0] + 1
        
	if (sys.argv[5] == "computer science" or sys.argv[5] == "cs" or sys.argv[5] == "CS" or sys.argv[5] == "ECE" or sys.argv[5] == "computer engineering"):
    		hint = 1
    		cat_to_promote[4] = cat_to_promote[4] + 1
   
	if (float(sys.argv[13]) < price_threshold):
    		hint = 1
    		cat_to_promote[5] = cat_to_promote[5] * .7
	else:
		cat_to_promote[5] = cat_to_promote[5] * 2

	if (float(sys.argv[7]) < age_threshold):
    		hint = 1
    		cat_to_promote[1] = cat_to_promote[1] * .6
	else:
	    	cat_to_promote[1] = cat_to_promote[1] * 1.5
    		cat_to_promote[0] = cat_to_promote[0] * .6
		
	if (sys.argv[9] == "male"):
	    	cat_to_promote[2] = cat_to_promote[2] * 1.5
	    
	if (sys.argv[9] == "female"):
	    	cat_to_promote[3] = cat_to_promote[3] * 1.5
	
	if(sys.argv[11] == "Supplies"):
	    	hint = 1
	    	cat_to_promote[1] = cat_to_promote[1] + .1
	elif(sys.argv[11] == "Merch"):
	    	hint = 1
	    	cat_to_promote[0] = cat_to_promote[0] + .1
	elif(sys.argv[11] == "Men"):
		hint = 1
	    	cat_to_promote[2] = cat_to_promote[2] + .1
	elif(sys.argv[11] == "Women"):
	    	hint = 1
	    	cat_to_promote[3] = cat_to_promote[3] + .1
	else:
	    	cat_to_promote[4] = cat_to_promote[4] + .1
	    
	if (hint == 0):
	    	cat_to_promote[5] = cat_to_promote[5] + 10
	    	
	if (sys.argv[14] == "clothes" or sys.argv[15] == "clothes" or sys.argv[16] == "clothes"):
	    if (sys.argv[9] == "male"):
	    	cat_to_promote[2] = cat_to_promote[2] * 1.5
	    if (sys.argv[9] == "female"):
	    	cat_to_promote[3] = cat_to_promote[3] * 1.5
    
	if (sys.argv[14] == "Illinois" or sys.argv[15] == "Illinois" or sys.argv[16] == "Illinois"):
        	cat_to_promote[0] = cat_to_promote[0] + .1
    
    	if (sys.argv[14] == "supplies" or sys.argv[15] == "supplies" or sys.argv[16] == "supplies"):
        	cat_to_promote[1] = cat_to_promote[1] + .5
    
    	if (sys.argv[14] == "programming" or sys.argv[15] == "programming" or sys.argv[16] == "programming"):
        	cat_to_promote[4] = cat_to_promote[4] + 10
        
	my_max = -100
	ind = 0
	for i in range(len(cat_to_promote)):
	    if (cat_to_promote[i]> my_max):
		    my_max = cat_to_promote[i]
		    ind = i
	
	if (ind == 0):	
		print("Merch")
	elif (ind == 1):
		print("Supplies")
	elif (ind == 2):
		print("Men")
	elif (ind == 3):
		print("Women")
	elif(ind == 4):
		print("Computer")
	else:
		print(sys.argv[11])


if __name__ == "__main__":main() ## with if
