<?php
define('DB_HOST', 'localhost');
define('DB_USER', 'iubookstore_calina');
define('DB_PASS', 'Hipporules2?');
define('DB_DB',   'iubookstore_bookstore');


class FUNCS
{

    function connect()
    {
        $con = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_DB);

				if ($con->connect_error) {
					die("Connection failed: " . $con->connect_error);
				}
				else
        	return $con;
    }
    
    function sanity()
    {
        die("sanity check");
    }

	function slashes($string)
	{
		return addcslashes($string, "'");
	}

	/*
	 *	Fetches an array from the query provided
	 *	Usage: $s->fetch_arry(QUERY);
	 */
    function fetch_array($query)
    {
        return mysqli_fetch_array($query);
	}

	/*
	 *	Fetches number of rows in the provided query
	 *	Usage: $s->num_rows(QUERY);
	 */
    function num_rows($query)
    {
        return mysqli_num_rows($query);
    }

	/*
	 *	Most generic version to do a query
	 *	Usage: $s->query(QUERY);
	 */
    function query($string)
    {
        global $con;

        if ($string)
            return mysqli_query($con, $string);
    }

	function real_escape($string)
	{
		global $con;
		return $con->real_escape_string($string);
	}

	function as($string) {
		return addslashes($string);
	}

	/*
	 *	Quickly and easily select a row from a table
	 *	Usage: $s->select(SELECT WHAT, TABLENAME, [WHERE, ORDER BY, LIMIT])
	 */
    function select($sel, $tbl, $wh = "", $or = "", $limit = "")
    {

        global $con;

        $query = "SELECT " . $sel . " FROM `" . $tbl . "`";


        if ($wh) {
            $query .= " WHERE " . $wh;
        }

        if ($or) {
            $query .= " ORDER BY " . $or;
        }

			if($limit) {
				$query .= " LIMIT " . $limit;
			}

			$query_request = $this->query($query);
			if($query_request)
        return $query_request;
			else
				return $query_request;

    }

	/*
	 *	Checks if a row exists within a table
	 *	Usage: $s->exists(TABLE_NAME, COLUMN, ARGS);
	 */
    function exists($tbl, $col, $arg, $arg2 = "")
    {
		global $con;

        $query = "SELECT * FROM `" . $tbl . "` WHERE `" . $col . "`='" . $arg . "'";

		if($arg2)
		{

			$query .= " " . $arg2;

		}

		$select = $this->query($query);

		if($select)
			$num = $this->num_rows($select);

		if($num > 0) {
			return true;
		}else{
			return false;
		}
    }

	/*
	 *	Insert data into a database
	 *	Usage: $s->insert(TABLE_NAME, VALUES);
	 */
    function insert($tbl, $args)
    {

        $query = "INSERT INTO `" . $tbl . "` VALUES (" . $args . ")";
        $query = $this->query($query);

        if ($query) {
            return $query;
        } else {
            return "INSERT INTO `" . $tbl . "` VALUES (" . $args . ")";
        }

    }

	/*
	 *	Updates a row in a table
	 *	Usage: $s->update(TABLE_NAME, ARGS, [WHERE, LIMIT])
	 */
	function update($tbl, $args, $where="", $limit="")
	{

		$query = "UPDATE " . $tbl . " SET " . $args;

		if($where) {
			$query .= " WHERE " . $where;
		}

		if($limit) {
			$query .= " LIMIT " . $limit;
		}

		return $this->query($query);

	}

}

	?>
