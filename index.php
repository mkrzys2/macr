<?
    /* DB FUNCTIONS */
    require_once('db.func.php');
    $d = new FUNCS();
    $con = $d->connect();

    $_best_sellers_select = $d->select("ID, Image, Name, Sales, Price", "Products", "", "Sales DESC", "3"); // Top 3 Best Selling Items

?>
<!DOCTYPE html>
<html lang="en">
<head>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>ui|bookstore</title>
  <meta name="description" content="UI Bookstore">
  <meta name="author" content="Calina Shaw, Maciej Krzysiak, Ayline Villegas, Ryan King">

  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="shortcut icon" href="https://web.illinois.edu:2083/brand/favicon.ico?1551750783" type="image/x-icon" />

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

</head>
<body>

  <div class="navbar-container row">
    <div class="brand one-half column">
      <a href="index.php">
        <div class="brand-image-section"><img src="https://creativeservices.illinois.edu/assets/img/imark.gif" width="35" alt="Logo Brand" /></div>
        <div class="brand-text-section"><span>UI</span><span class="light">BOOKSTORE</span></div>
      </a>
    </div>
    <div class="navbar one-half column">
      <ul class="navbar-list">
        <li><a class="active" href="index.php">Home</a></li>
        <li><a href="products.php">Products</a></li>
        <li class="mycart"><a href="mycart.php">My Cart</a></li>
        <li><a href="signin.php">Sign Up</a></li>
        <li><a href="teacher.php">Stats</a></li>
      </ul>
    </div>
  </div>

<div class="wrapper">
    <div class="section row">
      <div class="hero twelve columns">
        <div class="hero-text-section">
          <h2>Welcome to the bookstore.</h2>
          <div class="sub-heading">Scroll <span class="blue-text">below</span>, or click <span class="blue-text">above</span> to check out our <span class="underline">large selection</span> of items.</div>

          <div class="buttons-section">
            <div class="buttons blue solid"><a href="products.php">See All Products</a></div>
            <div class="buttons blue solid"><a href="#best-sellers">See Our Best Sellers</a></div>
          </div>
        <div class="arrow"></div>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="about one-half columns">
          <h3 class="orange-text">A LIL' ABOUT US</h3>
          <h4 class="blue-text">Just a small part of what we're all about.</h4>

          <div class="about-us-divider"></div>

          <div class="about-text-section">
            The UIUC Bookstore, located on Wright Street, offers a variety of products, services, and features for students of the University of Illinois at Urbana-Champaign as well as any other visitor who may come along. Among many products, we offer a variety of officially licensed Illini gear for every sporting event or day-to-day life. Additionally, we offer students the opportunity to rent, buy, or download textbooks for every class offered at the university.
          </div>
        </div>
      </div>

      <div id="best-sellers" class="row">
        <div class="top3 one-half columns">
          <h3 class="orange-text">OUR BEST SELLERS</h3>
          <h4 class="blue-text">Check out some of the best sellers.</h4>

          <div class="row">
            <?

                while($row = $d->fetch_array($_best_sellers_select)) {

                 echo "

                    <div class=\"product-item four columns\">
                      <a href=\"product_page.php?product_id={$row['ID']}\"><div class=\"product-item-image\" style=\"background: url('{$row['Image']}') center center no-repeat, #FFFFFF; background-size: 50%;\"></div></a>
                      <div class=\"product-item-title\"><a href=\"product_page.php?product_id={$row['ID']}\">{$row['Name']}</a> <div class=\"product-item-price\">\${$row['Price']}</div></div>
                    </div>

                 ";

                }

            ?>
          </div>
          <div class="row">
            <div class="buttons white solid"><a href="products.php">See All Products</a></div>
          </div>
        </div>
      </div>
    </div>

    <div class="footer row">
      <div class="twelve columns">
        <div class="row">
          <div class="three columns store-hours">
            <h3>Store hours</h3>
            <ul>
              <li><span class="emphasize">Monday</span>: CLOSED</li>
              <li><span class="emphasize">Tuesday</span>: 7:30a - 6:00p</li>
              <li><span class="emphasize">Wednesday</span>: 7:30a - 6:00p</li>
              <li><span class="emphasize">Thursday</span>: 7:30a - 6:00p</li>
              <li><span class="emphasize">Friday</span>: 7:30a - 6:00p</li>
              <li><span class="emphasize">Saturday</span>: 8:00a - 6:00p</li>
              <li><span class="emphasize">Sunday</span>: 8:00a - 6:00p</li>
            </ul>
          </div>
          <div class="three columns store-information">
            <div class="row">
              <div class="twelve columns">
                <h3>Getting here</h3>
                <div class="getting-here-information">
                  809 S. Wright Street Champaign, IL 61820
                </div>
              </div>
              <div class="bottom twelve columns">
                <h3>Call Us</h3>
                <div class="give-us-a-call-information">
                   (217) 333-2050
                </div>
              </div>
            </div>
          </div>
          <div class="six columns brand">
            <div class="brand-footer">
              <div class="brand-image-section"><img src="css/img/logo.png" width="35" alt="Logo Brand" /></div>
              <div class="brand-text-section"><span>UI</span><span class="light">BOOKSTORE</span></div>
            </div>
          </div>
        </div>
        <div class="row copyright">© 2019 Illini Union Bookstore | <a href="https://bookstore.illinois.edu/site_terms_of_use.asp">Privacy Policy</a> | <a href="https://bookstore.illinois.edu/site_terms_of_use.asp?#terms">Terms of Use</a></div>
      </div>
    </div>
  </div>

</body>
</html>
