<?php 

    require_once('db.func.php');
    $d = new FUNCS();
    $con = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_DB);

     if($_SERVER['REQUEST_METHOD']=='POST' && isset($_POST))
    { 
        session_start();
        $email = $_SESSION['email'];
        $c_id = 1;
        $info = $d->select("*", "Customer", "email = '$email'", "", "1");
        $row = $d->fetch_array($info);
        $cid = $row[0];
        $email = $row[1];
        $username = $row[2];
        $password = $row[3];
        $major = $row[4];
        $interests = $row[5];
        $age = $row[6];
        $grade = $row[7];
        $gender = $row[8];
        
        $sql = "SELECT * FROM Products INNER JOIN Purchased ON Products.ID=Purchased.pid WHERE Purchased.email='$email'";
        $items_bght = $d->query($sql);
        $row2 = $d->fetch_array($items_bght);
        $weight = -1;
        while($row2) {
            $pid = $row2[0];
            $price = $row2[4];
            $cat = $row2[7];
            $rel = $row2[8];
            $tag1 = $row2[9];
            $tag2 = $row2[10];
            $tag3 = $row2[11];

            exec("python /home/iubookstore/public_html/advone.py '$cid' '$email' '$username' '$password' '$major' '$interests' '$age' '$grade' '$gender' '$pid' '$cat' '$rel' '$price' '$tag1' '$tag2' '$tag3' '$weight'", $output);
            $cat = $output[0];
            if ($cat == "Merch") {
                $weight = 0;
            } else if ($cat == "Supplies") {
                $weight = 1;
            } else if ($cat == "Men") {
                $weight = 2;
            } else if ($cat == "Women") {
                $weight = 3;
            } else if ($cat == "Computer") {
                $weight = 4;
            } else {
                $weight = 5;
            }

            $row2 = $d->fetch_array($items_bght);
        }
        $cat = $output[0];
        
        $sql99 = "SELECT * FROM Products INNER JOIN Purchased ON Products.ID=Purchased.pid WHERE Purchased.email='$email'";
        $items_bght2 = $d->query($sql99);
        

        
        $cat_query = "SELECT * FROM Products WHERE Category='$cat' LIMIT 3";
        $_cat_items = $d->query($cat_query);
    } 
    
    # combines the purchase table with the customer table & gets all of the items that the customer has purchased
    # used for viewing a customer's cart
    session_start();
    $email = $_SESSION['email'];
    $sql = "SELECT * FROM Products INNER JOIN Purchased ON Products.ID=Purchased.pid WHERE Purchased.email='$email'";

    $_products_select = $d->query($sql);
?>

<!DOCTYPE html>
<html lang="en">
<head>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>ui|bookstore</title>
  <meta name="description" content="UI Bookstore">
  <meta name="author" content="Calina Shaw, Maciej Krzysiak, Ayline Villegas, Ryan King">

  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="shortcut icon" href="https://web.illinois.edu:2083/brand/favicon.ico?1551750783" type="image/x-icon" />

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

</head>
<body>

  <div class="navbar-container row">
    <div class="brand one-half column">
      <a href="index.php">
        <div class="brand-image-section"><img src="https://creativeservices.illinois.edu/assets/img/imark.gif" alt="Logo Brand" /></div>
        <div class="brand-text-section"><span>UI</span><span class="light">BOOKSTORE</span></div>
      </a>
    </div>
    <div class="navbar one-half column">
      <ul class="navbar-list">
        <li><a href="index.php">Home</a></li>
        <li><a href="products.php">Products</a></li>
        <li class="mycart"><a href="mycart.php">My Cart</a></li>
        <li><a href="signin.php">Sign Up</a></li>
        <li><a href="teacher.php">Stats</a></li>
      </ul>
    </div>
  </div>

  <!-- Maciej's garbo -->
  <div class="maciejs_garbo">
    <h3>Maciej's Dev Panel <p>(demonstration purposes only)</p></h3>
    <form method="post">
      <input class="buttons blue solid" type="submit" name="setToSoldOut" value="Update to SOLD OUT" />
      <input class="buttons blue solid" type="submit" name="deleteFromDB" value="DELETE (tpo)" />
      <input class="buttons blue solid" type="submit" name="inputIntoPurchases" value="Pseudo-Purchase (tpo)" />
      <div>
        <h3><p>Manipulate the quantity</p></h3>
        <input class="buttons blue solid" type="submit" name="subOne" value="Sub (-1) to Quantity" />
        <input class="buttons blue solid" type="submit" name="addOne" value="Add (+1) to Quantity" />
      </div>
    </form>
  </div>
  <!-- End of Maciej's garbo -->

  <!-- Dev panel -->

       <div class="wrapper">
    <div class="container">
      <div class="row">
        <div class="product-list twelve columns">
          <h3>You Selected:</h3>
          <h4>Your products will arrive in approximately: 9999999 years!</h4>
          <h5>Don't get too excited!</h5>
                <form method="post">
                    <div>
                    <h3><p>See Our Recommendations</p></h3>
                    <input class="buttons blue solid" type="submit" name="test" value="Surprise Me" />
                    </div>
                </form>
    
          <div class="row">
            <?

              while($row = $d->fetch_array($_products_select)) {
                echo "
                <!-- Product Item -->
                <div id=\"product-item-{$row['ID']}\" class=\"product-item four columns\">
                  <a href=\"product_page.php?product_id={$row['ID']}\">
                    <div class=\"product-item-image\" style=\"background: url('{$row['Image']}') center center no-repeat, #FFFFFF; background-size: 50%;\"></div>
                    <div class=\"product-item-title\">{$row['Name']} <div class=\"product-item-price\">\${$row['Price']}</div></div>
                  </a>
                </div>
                ";
              }
            ?>
          </div>
          <div class="row">
            <div class="prev_page four columns">«</div><div class="product-pagination-bullets four columns"></div><div class="next_page four columns">»</div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
        <!-- Products like this -->
<div id="best-sellers" class="row">
        <div class="top3 one-half columns">
          <h3 class="orange-text">CONSIDER</h3>
          <h4 class="blue-text">Purchasing!</h4>

          <div class="row">
            <?

                while($row = $d->fetch_array($_cat_items)) {

                 echo "

                    <div class=\"product-item four columns\">
                      <a href=\"product_page.php?product_id={$row['ID']}\"><div class=\"product-item-image\" style=\"background: url('{$row['Image']}') center center no-repeat, #FFFFFF; background-size: 50%;\"></div></a>
                      <div class=\"product-item-title\"><a href=\"product_page.php?product_id={$row['ID']}\">{$row['Name']}</a> <div class=\"product-item-price\">\${$row['Price']}</div></div>
                    </div>

                 ";

                }

            ?>
          </div>
        </div>
      </div>
    </div>

  <div class="footer row">
    <div class="twelve columns">
      <div class="row">
        <div class="three columns store-hours">
          <h3>Store hours</h3>
          <ul>
            <li><span class="emphasize">Monday</span>: CLOSED</li>
            <li><span class="emphasize">Tuesday</span>: 7:30a - 6:00p</li>
            <li><span class="emphasize">Wednesday</span>: 7:30a - 6:00p</li>
            <li><span class="emphasize">Thursday</span>: 7:30a - 6:00p</li>
            <li><span class="emphasize">Friday</span>: 7:30a - 6:00p</li>
            <li><span class="emphasize">Saturday</span>: 8:00a - 6:00p</li>
            <li><span class="emphasize">Sunday</span>: 8:00a - 6:00p</li>
          </ul>
        </div>
        <div class="three columns store-information">
          <div class="row">
            <div class="twelve columns">
              <h3>Getting here</h3>
              <div class="getting-here-information">
                809 S. Wright Street Champaign, IL 61820
              </div>
            </div>
            <div class="bottom twelve columns">
              <h3>Call Us</h3>
              <div class="give-us-a-call-information">
                 (217) 333-2050
              </div>
            </div>
          </div>
        </div>
        <div class="six columns brand">
          <div class="brand-footer">
            <div class="brand-image-section"><img src="css/img/logo.png" width="35" alt="Logo Brand" /></div>
            <div class="brand-text-section"><span>UI</span><span class="light">BOOKSTORE</span></div>
          </div>
        </div>
      </div>
      <div class="row copyright">© 2019 Illini Union Bookstore | <a href="https://bookstore.illinois.edu/site_terms_of_use.asp">Privacy Policy</a> | <a href="https://bookstore.illinois.edu/site_terms_of_use.asp?#terms">Terms of Use</a></div>
    </div>
  </div>

</body>
</html>