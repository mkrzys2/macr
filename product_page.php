<?
    /* Session */
    session_start();

    if($_GET) {
        $_SESSION['product'] = $_GET['product_id'];
        header('Location: product_page.php');
    }

    if(!$_SESSION['product']) {
        die('You do not have permission to view this page.');
    }


    $product_id = $_SESSION['product'];

    /* DB FUNCTIONS */
    require_once('db.func.php');

    /* Define the class functions used throughout */
    $d = new FUNCS();
    $con = $d->connect();
    
    if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['add2cart']))
    {
        $email = $_SESSION['email'];
        $sql = "INSERT INTO Purchased (email, pid) VALUES ('$email', '$product_id')";
        $d->query($sql);
        
        $sql2 = "UPDATE Products SET Sales=Sales+1 WHERE ID='$product_id'";
        $d->query($sql2);
        
        $sql3 = "UPDATE Products SET quantity=quantity-1 WHERE ID='$product_id'";
        $d->query($sql3);
    }

    if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['setToSoldOut']))
    {
        $sql = "UPDATE Products SET Name = 'SOLD OUT' WHERE id= '$product_id'";
        $d->query($sql);
        header('Location: product_page.php');

        // Alternatively: function update($tbl, $args, $where="", $limit="")
        // $d->update("Products", "Name='SOLD OUT', "id=1");
    }

    if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['deleteFromDB']))
    {
        $sql = "DELETE FROM Products WHERE id= '$product_id'";
        $d->query($sql);
        header('Location: product_page.php');
    }

    if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['inputIntoPurchases']))
    {
        $sql = "INSERT INTO Customer VALUES ('Garbage@temporary.com', 'abc', 'def')";
        $d->query($sql);
        $d->update("Products", "Sales=Sales+1", "ID='$product_id'");
        header('Location: product_page.php');
    }


    if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['subOne']))
    {
      $sql = "UPDATE Products SET Quantity = Quantity-1 WHERE id='$product_id'";
      $d->query($sql);
      header('Location: product_page.php');
    }

    if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['addOne']))
    {
      $sql = "UPDATE Products SET Quantity = Quantity+1 WHERE id='$product_id'";
      $d->query($sql);
      header('Location: product_page.php');
    }

    $_product_item = $d->select("*", "Products", "`ID` = {$product_id}", "", "1");
    $product_array = $d->fetch_array($_product_item);
?>
<!DOCTYPE html>
<html lang="en">
<head>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>ui|bookstore</title>
  <meta name="description" content="UI Bookstore">
  <meta name="author" content="Calina Shaw, Maciej Krzysiak, Ayline Villegas, Ryan King">

  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="shortcut icon" href="https://web.illinois.edu:2083/brand/favicon.ico?1551750783" type="image/x-icon" />

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

</head>
<body>

  <div class="navbar-container row">
    <div class="brand one-half column">
      <a href="index.php">
        <div class="brand-image-section"><img src="https://creativeservices.illinois.edu/assets/img/imark.gif" alt="Logo Brand" /></div>
        <div class="brand-text-section"><span>UI</span><span class="light">BOOKSTORE</span></div>
      </a>
    </div>
    <div class="navbar one-half column">
      <ul class="navbar-list">
        <li><a href="index.php">Home</a></li>
        <li><a href="products.php">Products</a></li>
        <li class="mycart"><a href="mycart.php">My Cart</a></li>
        <li><a href="signin.php">Sign Up</a></li>
        <li><a href="teacher.php">Stats</a></li>
      </ul>
    </div>
  </div>

  <!-- Maciej's garbo -->
  <div class="maciejs_garbo">
    <h3>Maciej's Dev Panel <p>(demonstration purposes only)</p></h3>
    <form method="post">
      <input class="buttons blue solid" type="submit" name="setToSoldOut" value="Update to SOLD OUT" />
      <input class="buttons blue solid" type="submit" name="deleteFromDB" value="DELETE (tpo)" />
      <input class="buttons blue solid" type="submit" name="inputIntoPurchases" value="Pseudo-Purchase (tpo)" />
      <div>
        <h3><p>Manipulate the quantity</p></h3>
        <input class="buttons blue solid" type="submit" name="subOne" value="Sub (-1) to Quantity" />
        <input class="buttons blue solid" type="submit" name="addOne" value="Add (+1) to Quantity" />
      </div>
    </form>
  </div>
  <!-- End of Maciej's garbo -->

  <!-- Dev panel -->


  <div class="wrapper">
    <div class="container">
      <div class="row">
        <div class="main-product twelve columns">
          <div class="row">
            <div class="four columns">
              <div class="product-page-image" style="background: url('<?=$product_array['Image']?>') center center no-repeat, #FFFFFF; background-size: 50%;"></div>
            </div>
            <div class="product-information eight columns">
              <div class="product-page-title blue-text"><?=$product_array['Name']?></div>
              <div class="stats_container">
                <div class="product-page-quantity">Quantity: <?=$product_array['Quantity']?></div>
                <div class="sales"><?=$product_array['Sales']?> sold already</div>
              </div>
              <div class="product-page-price">$<?=$product_array['Price']?></div>
              <form method="post">
              <input class="buttons blue solid" type="submit" name="add2cart" value="Add To Cart" />
              <input class="buttons blue solid" type="submit" name="deleteFromDB" value="DELETE (tpo)" />
              </form>
              <div class="product-page-description"><?=$product_array['Description']?></div>
            </div>
          </div>
        </div>
      </div>



  <div class="footer row">
    <div class="twelve columns">
      <div class="row">
        <div class="three columns store-hours">
          <h3>Store hours</h3>
          <ul>
            <li><span class="emphasize">Monday</span>: CLOSED</li>
            <li><span class="emphasize">Tuesday</span>: 7:30a - 6:00p</li>
            <li><span class="emphasize">Wednesday</span>: 7:30a - 6:00p</li>
            <li><span class="emphasize">Thursday</span>: 7:30a - 6:00p</li>
            <li><span class="emphasize">Friday</span>: 7:30a - 6:00p</li>
            <li><span class="emphasize">Saturday</span>: 8:00a - 6:00p</li>
            <li><span class="emphasize">Sunday</span>: 8:00a - 6:00p</li>
          </ul>
        </div>
        <div class="three columns store-information">
          <div class="row">
            <div class="twelve columns">
              <h3>Getting here</h3>
              <div class="getting-here-information">
                809 S. Wright Street Champaign, IL 61820
              </div>
            </div>
            <div class="bottom twelve columns">
              <h3>Call Us</h3>
              <div class="give-us-a-call-information">
                 (217) 333-2050
              </div>
            </div>
          </div>
        </div>
        <div class="six columns brand">
          <div class="brand-footer">
            <div class="brand-image-section"><img src="css/img/logo.png" width="35" alt="Logo Brand" /></div>
            <div class="brand-text-section"><span>UI</span><span class="light">BOOKSTORE</span></div>
          </div>
        </div>
      </div>
      <div class="row copyright">© 2019 Illini Union Bookstore | <a href="https://bookstore.illinois.edu/site_terms_of_use.asp">Privacy Policy</a> | <a href="https://bookstore.illinois.edu/site_terms_of_use.asp?#terms">Terms of Use</a></div>
    </div>
  </div>

</body>
</html>
