<?

/* DB FUNCTIONS */
require_once('db.func.php');
$d = new FUNCS();
$con = $d->connect();

$_products_select = $d->select("*", "Products"); // All items

?>
<!DOCTYPE html>
<html lang="en">
<head>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>ui|bookstore</title>
  <meta name="description" content="UI Bookstore">
  <meta name="author" content="Calina Shaw, Maciej Krzysiak, Ayline Villegas, Ryan King">

  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="shortcut icon" href="https://web.illinois.edu:2083/brand/favicon.ico?1551750783" type="image/x-icon" />

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

</head>
<body>

  <div class="navbar-container row">
    <div class="brand one-half column">
      <a href="index.php">
        <div class="brand-image-section"><img src="https://creativeservices.illinois.edu/assets/img/imark.gif" alt="Logo Brand" /></div>
        <div class="brand-text-section"><span>UI</span><span class="light">BOOKSTORE</span></div>
      </a>
    </div>
    <div class="navbar one-half column">
      <ul class="navbar-list">
        <li><a href="index.php">Home</a></li>
        <li><a class="active" href="products.php">Products</a></li>
        <li class="mycart"><a href="mycart.php">My Cart</a></li>
        <li><a href="signin.php">Sign Up</a></li>
        <li><a href="teacher.php">Stats</a></li>
      </ul>
    </div>
  </div>

  <div class="wrapper">
    <div class="container">
      <div class="row">
        <div class="product-list twelve columns">
          <h3>Browse our products</h3>
          <h4>Search is also available above.</h4>
          <div class="row">
            <?

              while($row = $d->fetch_array($_products_select)) {
                echo "
                <!-- Product Item -->
                <div id=\"product-item-{$row['ID']}\" class=\"product-item four columns\">
                  <a href=\"product_page.php?product_id={$row['ID']}\">
                    <div class=\"product-item-image\" style=\"background: url('{$row['Image']}') center center no-repeat, #FFFFFF; background-size: 50%;\"></div>
                    <div class=\"product-item-title\">{$row['Name']} <div class=\"product-item-price\">\${$row['Price']}</div></div>
                  </a>
                </div>
                ";
              }
            ?>
          </div>
          <div class="row">
            <div class="prev_page four columns">«</div><div class="product-pagination-bullets four columns"></div><div class="next_page four columns">»</div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="footer row">
    <div class="twelve columns">
      <div class="row">
        <div class="three columns store-hours">
          <h3>Store hours</h3>
          <ul>
            <li><span class="emphasize">Monday</span>: CLOSED</li>
            <li><span class="emphasize">Tuesday</span>: 7:30a - 6:00p</li>
            <li><span class="emphasize">Wednesday</span>: 7:30a - 6:00p</li>
            <li><span class="emphasize">Thursday</span>: 7:30a - 6:00p</li>
            <li><span class="emphasize">Friday</span>: 7:30a - 6:00p</li>
            <li><span class="emphasize">Saturday</span>: 8:00a - 6:00p</li>
            <li><span class="emphasize">Sunday</span>: 8:00a - 6:00p</li>
          </ul>
        </div>
        <div class="three columns store-information">
          <div class="row">
            <div class="twelve columns">
              <h3>Getting here</h3>
              <div class="getting-here-information">
                809 S. Wright Street Champaign, IL 61820
              </div>
            </div>
            <div class="bottom twelve columns">
              <h3>Call Us</h3>
              <div class="give-us-a-call-information">
                 (217) 333-2050
              </div>
            </div>
          </div>
        </div>
        <div class="six columns brand">
          <div class="brand-footer">
            <div class="brand-image-section"><img src="css/img/logo.png" width="35" alt="Logo Brand" /></div>
            <div class="brand-text-section"><span>UI</span><span class="light">BOOKSTORE</span></div>
          </div>
        </div>
      </div>
      <div class="row copyright">© 2019 Illini Union Bookstore | <a href="https://bookstore.illinois.edu/site_terms_of_use.asp">Privacy Policy</a> | <a href="https://bookstore.illinois.edu/site_terms_of_use.asp?#terms">Terms of Use</a></div>
    </div>
  </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script>

var product_length = $('.product-item').length;
var bullet_number = Math.floor((product_length/9) - 1);
var curr_page = 1;
var curr_start = 1;
var curr_end = 9;
$('.product-pagination-bullets').append('<span attr-page="1" class="page-click current pagination">1</span>');
$(".product-item").not(":nth-child(-n + 9)").css("display", "none");

for (var i = 0; i < bullet_number; i++) {
  $('.product-pagination-bullets').append('<span id="page-click" attr-page="'+(i+2)+'" class="page-click closed pagination">'+(i+2)+'</span>');
}

$('.page-click').on('click', function () {
  var page = $(this).attr('attr-page');
  if(curr_page == page) {
    return;
  }
  for(var x = curr_start; x <= curr_end; x++) {
    $('#product-item-' + x).hide();
  }
  curr_start = (page * 9) - 8;
  curr_end = (page * 9);
  for(var x = curr_start; x <= curr_end; x++) {
    $('#product-item-' + x).show();
  }
  $('.page-click[attr-page='+page+']').removeClass('closed').addClass('current');
  $('.page-click[attr-page='+curr_page+']').removeClass('current').addClass('closed');
  curr_page = page;
});

$('.next_page').on('click', function() {
  if(curr_page == (bullet_number+1) || bullet_number == -1) {
    return;
  }

  var page = curr_page + 1;

  for(var x = curr_start; x <= curr_end; x++) {
    $('#product-item-' + x).hide();
  }
  curr_start = (page * 9) - 8;
  curr_end = (page * 9);
  for(var x = curr_start; x <= curr_end; x++) {
    $('#product-item-' + x).show();
  }
  $('.page-click[attr-page='+page+']').removeClass('closed').addClass('current');
  $('.page-click[attr-page='+curr_page+']').removeClass('current').addClass('closed');
  curr_page = page;

});

$('.prev_page').on('click', function() {
  if(curr_page == 1) {
    return;
  }

  var page = curr_page - 1;

  for(var x = curr_start; x <= curr_end; x++) {
    $('#product-item-' + x).hide();
  }
  curr_start = (page * 9) - 8;
  curr_end = (page * 9);
  for(var x = curr_start; x <= curr_end; x++) {
    $('#product-item-' + x).show();
  }
  $('.page-click[attr-page='+page+']').removeClass('closed').addClass('current');
  $('.page-click[attr-page='+curr_page+']').removeClass('current').addClass('closed');
  curr_page = page;

});
</script>
</body>
</html>
