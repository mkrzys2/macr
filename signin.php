<?
define('DB_HOST', 'localhost');
define('DB_USER', 'iubookstore_calina');
define('DB_PASS', 'Hipporules2?');
define('DB_DB',   'iubookstore_bookstore');

    /* Session */
   // session_start();


    /* DB FUNCTIONS */
    require_once('db.func.php');
    $d = new FUNCS();
    $con = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_DB);
    
    if($_SERVER['REQUEST_METHOD']=='POST' && isset($_POST))
    { 
      
       $id = 0;
       $email = $_POST['email'];
       $username = $_POST['username'];
       $password = $_POST['password'];
       $major = $_POST['major'];
       $interests = $_POST['interests'];
       $age = $_POST['age'];
       $grade = $_POST['grade'];
       $gender = $_POST['gender'];
       
       $sql = "INSERT INTO Customer (cid, email, username, password, major, interests, age, grade, gender) VALUES ('$id', '$email', '$username', '$password',
       '$major', '$interests', '$age', '$grade', '$gender')";
        session_start();
        $_SESSION['email'] = $email;
        $_SESSION['username'] = $username;
        $link = mysqli_query($con, $sql);
        if(!$link)
        {
            header('Location: signin.php');
        } else {
            header('Location: index.php');
        }
      
    } 

    $_products_select = $d->select("*", "Products"); // All items
    
?>
<!DOCTYPE html>
<html lang="en">
<head>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>ui|bookstore</title>
  <meta name="description" content="UI Bookstore">
  <meta name="author" content="Calina Shaw, Maciej Krzysiak, Ayline Villegas, Ryan King">

  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="shortcut icon" href="https://web.illinois.edu:2083/brand/favicon.ico?1551750783" type="image/x-icon" />

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

</head>
<body>

  <div class="navbar-container row">
    <div class="brand one-half column">
      <a href="index.php">
        <div class="brand-image-section"><img src="https://creativeservices.illinois.edu/assets/img/imark.gif" alt="Logo Brand" /></div>
        <div class="brand-text-section"><span>UI</span><span class="light">BOOKSTORE</span></div>
      </a>
    </div>
    <div class="navbar one-half column">
      <ul class="navbar-list">
        <li><a href="index.php">Home</a></li>
        <li><a href="products.php">Products</a></li>
        <li class="mycart"><a href="mycart.php">My Cart</a></li>
        <li><a href="signin.php">Sign Up</a></li>
        <li><a href="teacher.php">Stats</a></li>
      </ul>
    </div>
  </div>

  <!-- Maciej's garbo -->
  <div class="maciejs_garbo">
    <h3>Maciej's Dev Panel <p>(demonstration purposes only)</p></h3>
    <form method="post">
      <input class="buttons blue solid" type="submit" name="setToSoldOut" value="Update to SOLD OUT" />
      <input class="buttons blue solid" type="submit" name="deleteFromDB" value="DELETE (tpo)" />
      <input class="buttons blue solid" type="submit" name="inputIntoPurchases" value="Pseudo-Purchase (tpo)" />
      <div>
        <h3><p>Manipulate the quantity</p></h3>
        <input class="buttons blue solid" type="submit" name="subOne" value="Sub (-1) to Quantity" />
        <input class="buttons blue solid" type="submit" name="addOne" value="Add (+1) to Quantity" />
      </div>
    </form>
  </div>
  <!-- End of Maciej's garbo -->

      <!-- Products like this -->
      <div class="row">
        <div class="top3 twelve columns">
          <div class="row">
            <div class="twelve columns">
                    <form method="post" action="signin.php">
                        <h5> Email </h5>
                        <input type="text" name="email" placeholder="JohnDoe@aol.com"><br>
                        <h5> Username </h5>
                        <input type="text" name="username" placeholder="noob1337"><br>
                        <h5> Password </h5>
                        <input type="text" name="password" placeholder="password123"><br>
                        <h5> Major </h5>
                        <input type="text" name="major" placeholder="computer science"><br>
                        <h5> Interests </h5>
                        <input type="text" name="interests" placeholder="chess, basketball, ..."><br>
                        <h5> Age </h5>
                        <input type="text" name="age" placeholder="37"><br>
                        <h5> Grade Level </h5>
                        <input type="text" name="grade" placeholder="sophmore"><br>
                        <h5> Gender </h5>
                        <input type="text" name="gender" placeholder="male"><br>
                        <input type="submit" value="Sign Up" name="signIn" />
                    </form>
            </div>
          </div>
          <div class="row">
            <div class="product-item four columns">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="footer row">
    <div class="twelve columns">
      <div class="row">
        <div class="three columns store-hours">
          <h3>Store hours</h3>
          <ul>
            <li><span class="emphasize">Monday</span>: CLOSED</li>
            <li><span class="emphasize">Tuesday</span>: 7:30a - 6:00p</li>
            <li><span class="emphasize">Wednesday</span>: 7:30a - 6:00p</li>
            <li><span class="emphasize">Thursday</span>: 7:30a - 6:00p</li>
            <li><span class="emphasize">Friday</span>: 7:30a - 6:00p</li>
            <li><span class="emphasize">Saturday</span>: 8:00a - 6:00p</li>
            <li><span class="emphasize">Sunday</span>: 8:00a - 6:00p</li>
          </ul>
        </div>
        <div class="three columns store-information">
          <div class="row">
            <div class="twelve columns">
              <h3>Getting here</h3>
              <div class="getting-here-information">
                809 S. Wright Street Champaign, IL 61820
              </div>
            </div>
            <div class="bottom twelve columns">
              <h3>Call Us</h3>
              <div class="give-us-a-call-information">
                 (217) 333-2050
              </div>
            </div>
          </div>
        </div>
        <div class="six columns brand">
          <div class="brand-footer">
            <div class="brand-image-section"><img src="css/img/logo.png" width="35" alt="Logo Brand" /></div>
            <div class="brand-text-section"><span>UI</span><span class="light">BOOKSTORE</span></div>
          </div>
        </div>
      </div>
      <div class="row copyright">© 2019 Illini Union Bookstore | <a href="https://bookstore.illinois.edu/site_terms_of_use.asp">Privacy Policy</a> | <a href="https://bookstore.illinois.edu/site_terms_of_use.asp?#terms">Terms of Use</a></div>
    </div>
  </div>

</body>
</html>