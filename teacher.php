<?
define('DB_HOST', 'localhost');
define('DB_USER', 'iubookstore_calina');
define('DB_PASS', 'Hipporules2?');
define('DB_DB',   'iubookstore_bookstore');

    /* Session */
    //session_start();

    /* DB FUNCTIONS */
    require_once('db.func.php');
    $d = new FUNCS();
    $con = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_DB);
    

    if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST))
    {
         $sql0 = "SELECT * FROM Customer";
         $out_of =  mysqli_num_rows($d->query($sql0));
        
        $sql1 = "SELECT * FROM Customer WHERE grade='freshmen'";
        $num_fresh =  mysqli_num_rows($d->query($sql1));
        $num_fresh_per = ($num_fresh / $out_of) * 100;
        
        $sql2 = "SELECT * FROM Customer WHERE grade='sophmore' OR grade='sophomore'";
        $num_soph =  mysqli_num_rows($d->query($sql2));
        $num_soph_per = ($num_soph / $out_of) * 100;
        
        $sql3 = "SELECT * FROM Customer WHERE grade='junior'";
        $num_junior =  mysqli_num_rows($d->query($sql3));
        $num_junior_per = ($num_junior / $out_of) * 100;
        
        $sql4 = "SELECT * FROM Customer WHERE grade='senior'";
        $num_senior =  mysqli_num_rows($d->query($sql4));
        $num_senior_per = ($num_senior / $out_of) * 100;
        
        $sql5 = "SELECT * FROM Customer WHERE gender='male'";
        $num_male =  mysqli_num_rows($d->query($sql5));
        $num_male_per = ($num_male / $out_of) * 100;
        
        $sql6 = "SELECT * FROM Customer WHERE gender='female'";
        $num_female =  mysqli_num_rows($d->query($sql6));
        $num_female_per = ($num_female / $out_of) * 100;
        
        $sql7 = "SELECT * FROM Customer WHERE age<=17";
        $num_under_17 =  mysqli_num_rows($d->query($sql7));
        $num_under_17_per = ($num_under_17 / $out_of) * 100;
        
        $sql8 = "SELECT * FROM Customer WHERE age=18";
        $num_18 =  mysqli_num_rows($d->query($sql8));
        $num_18_per = ($num_18 / $out_of) * 100;
        
        $sql9 = "SELECT * FROM Customer WHERE age=19";
        $num_19 =  mysqli_num_rows($d->query($sql9));
        $num_19_per = ($num_19 / $out_of) * 100;
        
        $sql10 = "SELECT * FROM Customer WHERE age=20";
        $num_20 =  mysqli_num_rows($d->query($sql10));
        $num_20_per = ($num_20 / $out_of) * 100;
        
        $sql11 = "SELECT * FROM Customer WHERE age>=21";
        $num_over_21 =  mysqli_num_rows($d->query($sql11));
        $num_over_21_per = ($num_over_21 / $out_of) * 100;
        
        
        # gets all of the customer majors and then selects the top 5
        
        $sql12 = "SELECT major, COUNT(major) AS occ FROM Customer GROUP BY major ORDER BY occ DESC";
        $res = $d->query($sql12);
        $top_major = mysqli_fetch_array($res);
        $top_major_per = ($top_major[1] / $out_of) * 100;

        $sec_major = mysqli_fetch_array($res);
        $sec_major_per = ($sec_major[1] / $out_of) * 100;

        $third_major = mysqli_fetch_array($res);
        $third_major_per = ($third_major[1] / $out_of) * 100;

        $fourth_major = mysqli_fetch_array($res);
        $fourth_major_per = ($fourth_major[1] / $out_of) * 100;
        
        $fifth_major = mysqli_fetch_array($res);
        $fifth_major_per = ($fifth_major[1] / $out_of) * 100;
        
        if ($_POST['keyword']) {
            $key = $_POST['keyword'];
            $tot_sales_statement = "SELECT * FROM Products WHERE (Sales>0) AND (tag1='$key' OR tag2='$key' OR tag3='$key')";
            $sql13 = $d->query($tot_sales_statement);
            $resulting = mysqli_fetch_array($sql13);
            $sales = 0;
            while($resulting) {
                $sales = $sales + $resulting[1];
                $resulting =  mysqli_fetch_array($sql13);
            }
            
            
            $sql14 = "SELECT id FROM Products WHERE (Sales > 0) AND (tag1='$key' OR tag2='$key' OR tag3='$key')";
            $pids = $d->query($sql14);
            $result_pids = mysqli_fetch_array($pids);
            $num_fresh_pids = 0;
            $num_soph_pids = 0;
            $num_junior_pids = 0;
            $num_senior_pids = 0;
            $num_tot_pids = 0;
            while($result_pids) {
                $curr_pid = $result_pids[0];
                
                
                # obtains the grade level of each student that bought a specific item
                # this is used for advanced function 2
                
                $sql15 = "SELECT Customer.grade FROM Customer WHERE email IN (SELECT Purchased.email FROM Products INNER JOIN Purchased ON Products.ID=Purchased.pid WHERE Purchased.pid ='$curr_pid')";
                $grades = $d->query($sql15);
                $grades_array = mysqli_fetch_array($grades);
                while($grades_array) {
                    if ($grades_array[0] == "freshmen") {
                        $num_fresh_pids = $num_fresh_pids + 1;
                    }
                    if ($grades_array[0] == "sophmore" || $grades_array[0] == "sophomore") {
                        $num_soph_pids = $num_soph_pids + 1;
                    }
                    if ($grades_array[0] == "junior") {
                        $num_junior_pids = $num_junior_pids + 1;
                    }
                    if ($grades_array[0] == "senior") {
                        $num_senior_pids = $num_senior_pids + 1;
                    }
                     $grades_array = mysqli_fetch_array($grades);
                }
                $result_pids =  mysqli_fetch_array($pids);
            }
            $num_tot_pids = $num_fresh_pids + $num_soph_pids + $num_junior_pids + $num_senior_pids;
            $num_fresh_pids_per = ($num_fresh_pids / $num_tot_pids) * 100;
            $num_soph_pids_per = ($num_soph_pids / $num_tot_pids) * 100;
            $num_junior_pids_per = ($num_junior_pids / $num_tot_pids) * 100;
            $num_senior_pids_per = ($num_senior_pids / $num_tot_pids) * 100;
            
            echo $num_fresh_pids;
            echo " ";
            echo $num_tot_pids;
            echo " ";
            echo $num_fresh_pids_per;
        }

    }

    $_products_select = $d->select("*", "Products"); // All items
    
?>
<!DOCTYPE html>
<html lang="en">
<head>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>ui|bookstore</title>
  <meta name="description" content="UI Bookstore">
  <meta name="author" content="Calina Shaw, Maciej Krzysiak, Ayline Villegas, Ryan King">

  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="shortcut icon" href="https://web.illinois.edu:2083/brand/favicon.ico?1551750783" type="image/x-icon" />

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

</head>
<body>

  <div class="navbar-container row">
    <div class="brand one-half column">
      <a href="index.php">
        <div class="brand-image-section"><img src="https://creativeservices.illinois.edu/assets/img/imark.gif" alt="Logo Brand" /></div>
        <div class="brand-text-section"><span>UI</span><span class="light">BOOKSTORE</span></div>
      </a>
    </div>
    <div class="navbar one-half column">
      <ul class="navbar-list">
        <li><a href="index.php">Home</a></li>
        <li><a href="products.php">Products</a></li>
        <li class="mycart"><a href="mycart.php">My Cart</a></li>
        <li><a href="signin.php">Sign Up</a></li>
        <li><a href="teacher.php">Stats</a></li>
      </ul>
    </div>
  </div>

  <!-- Maciej's garbo -->
  <div class="maciejs_garbo">
    <h3>Maciej's Dev Panel <p>(demonstration purposes only)</p></h3>
    <form method="post">
      <input class="buttons blue solid" type="submit" name="setToSoldOut" value="Update to SOLD OUT" />
      <input class="buttons blue solid" type="submit" name="deleteFromDB" value="DELETE (tpo)" />
      <input class="buttons blue solid" type="submit" name="inputIntoPurchases" value="Pseudo-Purchase (tpo)" />
      <div>
        <h3><p>Manipulate the quantity</p></h3>
        <input class="buttons blue solid" type="submit" name="subOne" value="Sub (-1) to Quantity" />
        <input class="buttons blue solid" type="submit" name="addOne" value="Add (+1) to Quantity" />
      </div>
    </form>
  </div>
  <!-- End of Maciej's garbo -->

  <!-- Dev panel -->

<div class="wrapper">
    <div class="section row">
      <div class="hero twelve columns">
        <div class="hero-text-section">
          <h2>Want to host an ad on our page?</h2>
          <h3>Interested if your idea for a class will be a hit?</h3>
          <div class="sub-heading">Scroll <span class="blue-text">below</span> and explore <span class="underline">page analytics</span>.</div>
          </div>
        <div class="arrow"></div>
        </div>
      </div>
    </div>

      <!-- Products like this -->
      <div class="row">
        <div class="top3 twelve columns">
          <div class="row">
            <div class="twelve columns">
                    <form action="teacher.php" method="post">
                        <h5>Optional: Enter a keyword below to see analytics surrounding it. Otherwise, hit guage and explore.</h5>
                        <h5 class="blue-text">Keyword:</h5>
                       <input type="text" name="keyword" placeholder="programming"><br>
                        <input class="buttons blue solid" type="submit" name="guage" value="Guage Interest" />
                    </form>
            </div>
          </div>
          <div class="row">
            <div class="product-item four columns">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  

<h5>The keyword you searched for: "<?php echo $key?>" had <?php echo $sales?> sales associated with it.</h5>

  
<br></br>
 
    <div class="wrapper">
    <div class="section row">
      <div class="hero twelve columns">
        <div class="hero-text-section">
            <h1>Optional Search Visualized</h1>
          <div class="sub-heading">Freshmen vs. Sophomores vs. Juniors vs. Seniors</div>
          </div>
        </div>
      </div>
    </div>
  
  <ul class="bar-graph">
  
  <li class="bar primary" style="height: <?php echo $num_fresh_pids_per?>%;" title="<?php echo $num_fresh_pids_per?>">
    <div class="percent"><?php echo $num_fresh_pids_per?>%<span></span></div>
    <div class="description">Freshmen</div>
  </li>
  <li class="bar secondary" style="height: <?php echo $num_soph_pids_per?>%;" title="<?php echo $num_soph_pids_per?>">
    <div class="percent"><?php echo $num_soph_pids_per?>%<span></span></div>
    <div class="description">Sophomores</div>
  </li>
  <li class="bar success" style="height: <?php echo $num_junior_pids_per?>%;" title="<?php echo $num_junior_pids_per?>">
    <div class="percent"><?php echo $num_junior_pids_per?>%<span></span></div>
    <div class="description">Juniors</div>
  </li>
  <li class="bar warning" style="height: <?php echo $num_senior_pids_per?>%;" title="<?php echo $num_senior_pids_per?>">
    <div class="percent"><?php echo $num_senior_pids_per?>%<span></span></div>
    <div class="description">Seniors</div>
  </li>
</ul>

<br></br>
<br></br>
<br></br>

    <div class="wrapper">
    <div class="section row">
      <div class="hero twelve columns">
        <div class="hero-text-section">
            <h1>Overall Website Grade Demographics</h1>
          <div class="sub-heading">Freshmen vs. Sophomores vs. Juniors vs. Seniors</div>
          </div>
        </div>
      </div>
    </div>

  <ul class="bar-graph">
  
  <li class="bar primary" style="height: <?php echo $num_fresh_per?>%;" title="<?php echo $num_fresh_per?>">
    <div class="percent"><?php echo $num_fresh?><span></span></div>
    <div class="description">Freshmen</div>
  </li>
  <li class="bar secondary" style="height: <?php echo $num_soph_per?>%;" title="<?php echo $num_soph_per?>">
    <div class="percent"><?php echo $num_soph?><span></span></div>
    <div class="description">Sophomores</div>
  </li>
  <li class="bar success" style="height: <?php echo $num_junior_per?>%;" title="<?php echo $num_junior_per?>">
    <div class="percent"><?php echo $num_junior?><span></span></div>
    <div class="description">Juniors</div>
  </li>
  <li class="bar warning" style="height: <?php echo $num_senior_per?>%;" title="<?php echo $num_senior_per?>">
    <div class="percent"><?php echo $num_senior?><span></span></div>
    <div class="description">Seniors</div>
  </li>
</ul>

<br></br>
<br></br>
<br></br>

<div class="wrapper">
    <div class="section row">
      <div class="hero twelve columns">
        <div class="hero-text-section">
            <h1>Gender Demographics</h1>
          <div class="sub-heading">Men vs. Women</div>
          </div>
        </div>
      </div>
    </div>

  <ul class="bar-graph">
  
  <pre>                                    </pre>
  <li class="bar secondary" style="height: <?php echo $num_male_per?>%;" title="<?php echo $num_male?>">
    <div class="percent"><?php echo $num_male?><span></span></div>
    <div class="description">Male</div>
  </li>
  <li class="bar success" style="height: <?php echo $num_female_per?>%;" title="<?php echo $num_female?>">
    <div class="percent"><?php echo $num_female?><span></span></div>
    <div class="description">Female</div>
  </li>
</ul>

  <br></br>
<br></br>
<br></br>

    <div class="wrapper">
    <div class="section row">
      <div class="hero twelve columns">
        <div class="hero-text-section">
            <h1>Age Demographics</h1>
          <div class="sub-heading"><=17 vs. 18 vs 19 vs. 20 vs >=21</div>
          </div>
        </div>
      </div>
    </div>
  
  <ul class="bar-graph">
  
  <li class="bar primary" style="height: <?php echo $num_under_17_per?>%;" title="<?php echo $num_under_17?>">
    <div class="percent"><?php echo $num_under_17?><span></span></div>
    <div class="description">17 and younger</div>
  </li>
  <li class="bar secondary" style="height: <?php echo $num_18_per?>%;" title="<?php echo $num_18?>">
    <div class="percent"><?php echo $num_18?><span></span></div>
    <div class="description">18 Years Old</div>
  </li>
  <li class="bar success" style="height: <?php echo $num_19_per?>%;" title="<?php echo $num_19?>">
    <div class="percent"><?php echo $num_19?><span></span></div>
    <div class="description">19 Years Old</div>
  </li>
  <li class="bar warning" style="height: <?php echo $num_20_per?>%;" title="<?php echo $num_20?>">
    <div class="percent"><?php echo $num_20?><span></span></div>
    <div class="description">20 Years Old</div>
  </li>
  <li class="bar alert" style="height: <?php echo $num_over_21_per?>%;" title="<?php echo $num_over_21?>">
    <div class="percent"><?php echo $num_over_21?><span></span></div>
    <div class="description">21 Years Old</div>
  </li>
</ul>

  <br></br>
<br></br>
<br></br>
 
    <div class="wrapper">
    <div class="section row">
      <div class="hero twelve columns">
        <div class="hero-text-section">
            <h1>Majors Demographics</h1>
          <div class="sub-heading">Top 5 Majors</div>
          </div>
        </div>
      </div>
    </div>
    
      <ul class="bar-graph">
  
  <li class="bar primary" style="height: <?php echo $top_major_per?>%;" title="<?php echo $top_major[0]?>">
    <div class="percent"><?php echo $top_major[0]?><span></span></div>
    <div class="description">1st</div>
  </li>
  <li class="bar secondary" style="height: <?php echo $sec_major_per?>%;" title="<?php echo $sec_major[0]?>">
    <div class="percent"><?php echo $sec_major[0]?><span></span></div>
    <div class="description">2nd</div>
  </li>
  <li class="bar success" style="height: <?php echo $third_major_per?>%;" title="<?php echo $third_major[0]?>">
    <div class="percent"><?php echo $third_major[0]?><span></span></div>
    <div class="description">3rd</div>
  </li>
  <li class="bar warning" style="height: <?php echo $fourth_major_per?>%;" title="<?php echo $fourth_major[0]?>">
    <div class="percent"><?php echo $fourth_major[0]?><span></span></div>
    <div class="description">4th</div>
  </li>
  <li class="bar alert" style="height: <?php echo $fifth_major_per?>%;" title="<?php echo $fifth_major[0]?>">
    <div class="percent"><?php echo $fifth_major[0]?><span></span></div>
    <div class="description">5th</div>
  </li>
</ul>






  <div class="footer row">
    <div class="twelve columns">
      <div class="row">
        <div class="three columns store-hours">
          <h3>Store hours</h3>
          <ul>
            <li><span class="emphasize">Monday</span>: CLOSED</li>
            <li><span class="emphasize">Tuesday</span>: 7:30a - 6:00p</li>
            <li><span class="emphasize">Wednesday</span>: 7:30a - 6:00p</li>
            <li><span class="emphasize">Thursday</span>: 7:30a - 6:00p</li>
            <li><span class="emphasize">Friday</span>: 7:30a - 6:00p</li>
            <li><span class="emphasize">Saturday</span>: 8:00a - 6:00p</li>
            <li><span class="emphasize">Sunday</span>: 8:00a - 6:00p</li>
          </ul>
        </div>
        <div class="three columns store-information">
          <div class="row">
            <div class="twelve columns">
              <h3>Getting here</h3>
              <div class="getting-here-information">
                809 S. Wright Street Champaign, IL 61820
              </div>
            </div>
            <div class="bottom twelve columns">
              <h3>Call Us</h3>
              <div class="give-us-a-call-information">
                 (217) 333-2050
              </div>
            </div>
          </div>
        </div>
        <div class="six columns brand">
          <div class="brand-footer">
            <div class="brand-image-section"><img src="css/img/logo.png" width="35" alt="Logo Brand" /></div>
            <div class="brand-text-section"><span>UI</span><span class="light">BOOKSTORE</span></div>
          </div>
        </div>
      </div>
      <div class="row copyright">© 2019 Illini Union Bookstore | <a href="https://bookstore.illinois.edu/site_terms_of_use.asp">Privacy Policy</a> | <a href="https://bookstore.illinois.edu/site_terms_of_use.asp?#terms">Terms of Use</a></div>
    </div>
  </div>

</body>
</html>